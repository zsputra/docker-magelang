import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './shared/app-material.module';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HeaderComponent } from './header/header.component';
import { BooksComponent } from './books/books.component';
import { BookDetailComponent } from './books/book-detail/book-detail.component';
import { BookEditComponent } from './books/book-edit/book-edit.component';
import { BookListComponent } from './books/book-list/book-list.component';
import { WritersComponent } from './writers/writers.component';
import { WriterDetailComponent } from './writers/writer-detail/writer-detail.component';
import { WriterEditComponent } from './writers/writer-edit/writer-edit.component';
import { WriterListComponent } from './writers/writer-list/writer-list.component';
import { BookAddComponent } from './books/book-add/book-add.component';
import { WriterAddComponent } from './writers/writer-add/writer-add.component';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    BooksComponent,
    BookDetailComponent,
    BookEditComponent,
    BookListComponent,
    WritersComponent,
    WriterDetailComponent,
    WriterEditComponent,
    WriterListComponent,
    BookAddComponent,
    WriterAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
