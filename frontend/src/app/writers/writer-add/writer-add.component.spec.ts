import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WriterAddComponent } from './writer-add.component';

describe('WriterAddComponent', () => {
  let component: WriterAddComponent;
  let fixture: ComponentFixture<WriterAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WriterAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WriterAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
