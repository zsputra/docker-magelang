import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { BookDetailComponent } from './books/book-detail/book-detail.component';
import { WritersComponent } from './writers/writers.component';
import { WriterDetailComponent } from './writers/writer-detail/writer-detail.component';

const routes: Routes = [
  { path: 'books', component: BooksComponent, children: [
    { path: '', component: BooksComponent },
    { path: ':id', component: BookDetailComponent }
  ] },
  { path: 'writers', component: WritersComponent, children: [
    { path: ':id', component: WriterDetailComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
