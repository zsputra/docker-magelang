package com.emerio.penulisservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PenulisServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PenulisServiceApplication.class, args);
	}

}
